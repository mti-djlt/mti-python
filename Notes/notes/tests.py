from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from .models import Category, Note


class CategoryTests(APITestCase):
    def setUp(self):
        user = User.objects.create_user('User', "user@gmail.com", 'password')

        category = Category(name="Initial Category", user=user)
        category.save()

    def test_categories_get(self):
        self.client.login(username='User', password='password')
        url = reverse('categories')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], "Initial Category")

    def test_categories_post(self):
        self.client.login(username='User', password='password')
        url = reverse('categories')
        data = {
            "name": "Post Category"
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, 201)
        category_id = response.data.get("id")
        category = Category.objects.get(id=category_id)
        self.assertEqual(category.name, "Post Category")

    def test_category_details_get(self):
        self.client.login(username='User', password='password')
        category = Category.objects.get()
        url = reverse('categories_detail', kwargs={"pk": category.id})

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], "Initial Category")

    def test_category_details_update(self):
        self.client.login(username='User', password='password')
        category = Category.objects.get()
        url = reverse('categories_detail', kwargs={"pk": category.id})
        data = {
            "name": "Modified Category"
        }

        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, 200)
        category = Category.objects.get()
        self.assertEqual(category.name, "Modified Category")

    def test_category_details_delete(self):
        self.client.login(username='User', password='password')
        category = Category.objects.get()
        url = reverse('categories_detail', kwargs={"pk": category.id})

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)
        self.assertEqual(Category.objects.count(), 0)

class NoteTests(APITestCase):
    def setUp(self):
        user = User.objects.create_user('User', "user@gmail.com", 'password')

        category = Category(name="Initial Category", user=user)
        category.save()

        note = Note(title="Initial Note", body="This is a note test",
                        category=category, user=user)
        note.save()

    def test_notes_get(self):
        self.client.login(username='User', password='password')
        url = reverse('notes')

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['title'], "Initial Note")
        self.assertEqual(response.data[0]['body'], "This is a note test")

    def test_categories_post(self):
        self.client.login(username='User', password='password')
        url = reverse('notes')
        category = Category.objects.get()
        data = {
            "title": "Post Note",
            "body": "Test note",
            "category": category.id
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, 201)
        note_id = response.data.get("id")
        note = Note.objects.get(id=note_id)
        self.assertEqual(note.title, "Post Note")
        self.assertEqual(note.body, "Test note")

    def test_category_details_get(self):
        self.client.login(username='User', password='password')
        note = Note.objects.get()
        url = reverse('notes_detail', kwargs={"pk": note.id})

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["title"], "Initial Note")
        self.assertEqual(response.data["body"], "This is a note test")

    def test_category_details_update(self):
        self.client.login(username='User', password='password')
        note = Note.objects.get()
        category = Category.objects.get()
        url = reverse('notes_detail', kwargs={"pk": note.id})
        data = {
            "title": "Modified Note",
            "body": "Test note modified",
            "category": category.id
        }

        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, 200)
        note = Note.objects.get()
        self.assertEqual(note.title, "Modified Note")
        self.assertEqual(note.body, "Test note modified")

    def test_category_details_delete(self):
        self.client.login(username='User', password='password')
        note = Note.objects.get()
        url = reverse('notes_detail', kwargs={"pk": note.id})

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 204)
        self.assertEqual(Note.objects.count(), 0)
