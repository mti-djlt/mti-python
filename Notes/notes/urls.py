from django.urls import path

from . import views

urlpatterns = [
    path('categories/', views.CategoryList.as_view(), name='categories'),
    path('categories/<int:pk>/', views.CategoryDetail.as_view(), name='categories_detail'),
    path('notes/', views.NoteList.as_view(), name='notes'),
    path('notes/<int:pk>/', views.NoteDetail.as_view(), name='notes_detail'),
    path('categories/<int:category_id>/notes', views.NoteCategory.as_view(), name='category_notes_get'),
    path('notes/<int:pk>/pdf', views.NotePdf.as_view(), name='notes_pdf'),
    path('notes/<int:pk>/docx', views.NoteDocx.as_view(), name='notes_docx'),
]