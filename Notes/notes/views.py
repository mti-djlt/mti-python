from django.db import transaction, IntegrityError
from django.db.models import Q
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.views import APIView
from easy_pdf.rendering import render_to_pdf_response
from docx import Document

from .models import Category, Note
from .serializers import CategorySerializer, NoteSerializer


class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def list(self, request, *args, **kwargs):
        categories = Category.objects.filter(Q(user=None) | Q(user_id=request.user.pk))
        serializer = CategorySerializer(categories, many=True)

        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        data['user_id'] = request.user.pk

        serializer = CategorySerializer(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        try:
            with transaction.atomic():
                category = serializer.save()

                if isinstance(category, Category):
                    category.user = request.user
                    category.save()

        except IntegrityError:
            errors = {
                "error": "couple (category, user) should be unique."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status.HTTP_201_CREATED)


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            category = Category.objects.get(id=pk)
            if category.user is not None and category.user_id != request.user.pk:
                raise Category.DoesNotExist
        except Category.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        serializer = CategorySerializer(category)

        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }

            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            category = Category.objects.get(id=pk, user_id=request.user.pk)
        except Category.DoesNotExist:
            if Category.objects.filter(id=pk, user=None).exists():
                errors = {
                    "error": "you don't have the rights to update."
                }
                ret_status = status.HTTP_400_BAD_REQUEST
            else:
                errors = {
                    "error": "id not found."
                }
                ret_status = status.HTTP_404_NOT_FOUND

            return Response(errors, ret_status)

        serializer = CategorySerializer(category, data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        try:
            serializer.save()
        except IntegrityError:
            errors = {
                "error": "couple (category, user) should be unique."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            category = Category.objects.get(id=pk, user_id=request.user.pk)
        except Category.DoesNotExist:
            if Category.objects.filter(id=pk, user=None).exists():
                errors = {
                    "error": "you don't have the rights to update."
                }
                ret_status = status.HTTP_400_BAD_REQUEST
            else:
                errors = {
                    "error": "id not found."
                }
                ret_status = status.HTTP_404_NOT_FOUND

            return Response(errors, ret_status)

        category.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class NoteList(generics.ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def list(self, request, *args, **kwargs):
        notes = Note.objects.filter(user_id=request.user.pk)
        serializer = NoteSerializer(notes, many=True)

        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        data['user_id'] = request.user.pk

        serializer = NoteSerializer(data=data)

        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        try:
            category = Category.objects.get(id=data['category'])
            if category.user is not None and category.user_id != request.user.pk:
                raise Category.DoesNotExist
        except Category.DoesNotExist:
            errors = {
                "category": ["Invalid pk " + str(request.data['category']) + " - object does not exist."]
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            with transaction.atomic():
                note = serializer.save()

                if isinstance(note, Note):
                    note.user = request.user
                    note.save()

        except IntegrityError:
            errors = {
                "error": "couple (note, user) should be unique."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status.HTTP_201_CREATED)


class NoteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            note = Note.objects.get(id=pk, user_id=request.user.pk)
        except Note.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        serializer = NoteSerializer(note)

        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            note = Note.objects.get(id=pk, user_id=request.user.pk)
        except Note.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        serializer = NoteSerializer(note, data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        try:
            category = Category.objects.get(id=request.data['category'])
            if category.user is not None and category.user_id != request.user.pk:
                raise Category.DoesNotExist
        except Category.DoesNotExist:
            errors = {
                "category": ["Invalid pk " + str(request.data['category']) + " - object does not exist."]
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            serializer.save()
        except IntegrityError:
            errors = {
                "error": "couple (category, user) should be unique."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None or not isinstance(pk, int):
            errors = {
                "error": "id should be integer."
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        try:
            note = Note.objects.get(id=pk, user_id=request.user.pk)
        except Note.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        note.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class NoteCategory(APIView):
    def get(self, request, category_id):
        notes = Note.objects.filter(user_id=request.user.pk, category_id=category_id)
        serializer = NoteSerializer(notes, many=True)

        return Response(serializer.data)


class NotePdf(APIView):
    def get(self, request, pk):
        try:
            note = Note.objects.get(id=pk, user_id=request.user.pk)
        except Note.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        template = 'templates/pdf_model.html'
        context = {'Note': note}

        return render_to_pdf_response(request, template, context, filename=str(note) + '.pdf')


class NoteDocx(APIView):
    def get(self, request, pk):
        try:
            note = Note.objects.get(id=pk, user_id=request.user.pk)
        except Note.DoesNotExist:
            errors = {
                "error": "id not found."
            }
            return Response(errors, status.HTTP_404_NOT_FOUND)

        document = Document()

        document.add_heading(note.title, 0)

        document.add_paragraph(note.body)

        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        response['Content-Disposition'] = 'attachment; filename=download.docx'

        document.save(response)

        return response
