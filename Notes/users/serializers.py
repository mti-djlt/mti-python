from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.Serializer):
    username = serializers.EmailField()
    password = serializers.CharField(max_length=100)

    def create(self, validated_data):
        user = User.objects.create_user(username=self.validated_data['username'], password=self.validated_data['password'])
        return user


class RefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField(max_length=100)


class TokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=100)
