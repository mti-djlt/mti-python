from django.contrib.auth.models import User
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .serializers import UserSerializer, TokenSerializer, RefreshTokenSerializer

import requests

CLIENT_ID = 'jtTPhUZrONidHDVN69a5KNzo4EcKczu6fNKOzu0e'
CLIENT_SECRET = 'HBFOQx6jziXB9Aa5fuRibrp9bVPLWhO3TfXJB6YhYXFC3wXS3FQwISVA9dBgl9MxHJg8hvIdOiZ4xOWCb0TyVgDwzoAev5J7Z3OEAIQPgP2V4CBCeVj82MKbdoN3bUab'

FBCLIENT_ID = 'ztwWW8eljOvuabRNuUTDiU06fLssjEn6l7zD2tPf'
FBCLIENT_SECRET = 'P1jFNvY2zA0pud2pi0xPPMgkFCIVn3LpFagthhTC3VzNCePbZJp9qVou1TzZfGsPIRDaEa4XmUe9Y84T1rezDuZnIoXBmglxCmZbULNpmE0ePziWhAAGhm9xR7Iig269'


@swagger_auto_schema(methods=['post'], request_body=UserSerializer, responses={201: "Created", 400: "Bad Request"})
@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    serializer = UserSerializer(data=request.data)

    if serializer.is_valid():
        if User.objects.filter(username=request.data['username']).exists():
            errors = {
                "username": "A user with that username already exists.",
            }
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        serializer.save()
    else:
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    r = requests.post('http://0.0.0.0:8000/o/token/',
                      data={
                          'grant_type': 'password',
                          'username': request.data['username'],
                          'password': request.data['password'],
                          'client_id': CLIENT_ID,
                          'client_secret': CLIENT_SECRET,
                      },
                      )

    return Response(r.json(), status=status.HTTP_201_CREATED)


@swagger_auto_schema(methods=['post'], request_body=UserSerializer, responses={200: "Ok", 400: "Bad Request"})
@api_view(['POST'])
@permission_classes([AllowAny])
def token(request):
    serializer = UserSerializer(data=request.data)

    if not serializer.is_valid():
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    username = request.data.get('username')
    password = request.data.get('password')

    r = requests.post(
        'http://0.0.0.0:8000/o/token/',
        data={
            'grant_type': 'password',
            'username': username,
            'password': password,
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )

    if r.status_code == requests.codes.ok:
        return Response(r.json())

    return Response(r.json(), status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(methods=['post'], request_body=RefreshTokenSerializer, responses={200: "Ok", 400: "Bad Request"})
@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    serializer = RefreshTokenSerializer(data=request.data)

    if not serializer.is_valid():
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    refresh_token = request.data.get('refresh_token')

    r = requests.post(
        'http://0.0.0.0:8000/o/token/',
        data={
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )

    if r.status_code == requests.codes.ok:
        return Response(r.json())

    return Response(r.json(), status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(methods=['post'], request_body=TokenSerializer, responses={204: "Accepted"})
@api_view(['POST'])
@permission_classes([AllowAny])
def revoke_token(request):
    serializer = TokenSerializer(data=request.data)

    if not serializer.is_valid():
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    token = request.data.get('token')

    r = requests.post(
        'http://0.0.0.0:8000/o/revoke-token/',
        data={
            'token': token,
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )

    return Response(status=r.status_code)
