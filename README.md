# mti-python
Command to run REST API that handles notes and their categories:

- python3.6 -m venv .env --without-pip
- source .env/bin/activate
- wget https://bootstrap.pypa.io/get-pip.py
- python get-pip.py
- rm get-pip.py
- pip install -r requirements.txt
- cd Notes/
- python manage.py makemigrations
- python manage.py migrate
- python manage.py runserver


To launch the swagger, go to:
- http://localhost:8000/api/doc

Authorization routes to register, retrieve, revoke...
are allowed to anybody.

Once you retrieved your access_token, you can put it in the authorize
of the swagger:
- If you retrieved the access_token from our api:
    - 'Bearer ' + 'access_token'
- If you retrieved the access_token from facebook:
    - 'Bearer facebook ' + 'access_token'
- If you retrieved the access_token from google:
    - 'Bearer google-oauth2 ' + 'access_token'

The rest of the routes are explicit and have to have 
the access_token in Authorization headers.



Command to run front website to test the API:

- cd soservices/
- npm i
- npm run serve