import Vue from 'vue'
import Vuex from 'vuex'
import { stat } from 'fs';
Vue.use(Vuex)

const state = {
    isConnected: false,
    token: "",
    currentLat: 0,
    currentLong: 0,
    besoin_recherché: "",
    categorie_besoin: [],
    duree: "",
    distance: "",
    lieu:null,
    url: "tst"
}

const mutations = {
    CHANGE_IS_CONNECTED: (state, etat) => {
        state.isConnected = etat
    },

    CHANGE_TOKEN: (state, token) => {
        state.token = token
    },

    RECHERCHE_RAPIDE_BR: (state, br) => {
        state.besoin_recherché = br;
    },

    RECHERCHE_RAPIDE_CB: (state, cb) => {
        state.categorie_besoin = cb;
    },

    RECHERCHE_RAPIDE_Duree: (state, duree) => {
        state.duree = duree;
    },

    RECHERCHE_RAPIDE_Distance: (state, distance) => {
        state.distance = distance;
    },

    RECHERCHE_RAPIDE_Lieu: (state, lieu) => {
        state.lieu = lieu;
    },

    CHANGE_CURR_LAT: (state, lat) => {
        state.currentLat = lat;
       
    },

    CHANGE_CURR_LONG: (state, long) => {
        state.currentLong = long;
    },

    CHANGE_URL: (state, url) => {
        state.url = url;
    },    
}

const getters = {
    isConnected: state => state.isConnected,
    token: state => state.token,
    besoin_recherché: state => state.besoin_recherché,
    categorie_besoin: state => state.categorie_besoin,
    durée: state => state.durée,
    distance: state => state.distance,
    lieu: state => state.lieu,
    lat: state => state.currentLat,
    long: state => state.currentLong,
    url: state => state.url
}

const actions = {
    ChangeIsConnected: (store, etat) => {
        store.commit('CHANGE_IS_CONNECTED', etat)

    },

    ChangeToken: (store, token) => {
        store.commit('CHANGE_TOKEN', token)
    },

    RechercheRapideBR: (store, br) => {
        store.commit('RECHERCHE_RAPIDE_BR', br)
    },

    RechercheRapideCB: (store, cb) => {
        store.commit('RECHERCHE_RAPIDE_CB', cb)
    },

    RechercheRapideDuree: (store, duree) => {
        store.commit('RECHERCHE_RAPIDE_Duree', duree)
    },

    RechercheRapideDistance: (store, distance) => {
        store.commit('RECHERCHE_RAPIDE_Distance', distance)
    },

    RechercheRapideLieu: (store, lieu) => {
        store.commit('RECHERCHE_RAPIDE_Lieu', lieu)
    },

    ChangeCurrLat: (store, lat) => {
        store.commit('CHANGE_CURR_LAT', lat)
    },

    ChangeCurrLong: (store, long) => {
        store.commit('CHANGE_CURR_LONG', long)
    },

    ChangeUrl: (store, url) => {
        store.commit('CHANGE_URL', url)
    },
}

let store = new Vuex.Store({
    state: state,
    mutations: mutations,
    getters: getters,
    actions: actions
})

global.store = store
export default store