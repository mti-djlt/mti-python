//<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>
import $ from "jquery";
import store from "./SOServicesStore";
import Vuex from "vuex";

$.loadScript = function (url, callback) {
  $.ajax({
    url: url,
    dataType: 'script',
    success: callback,
    async: true
  });
}

var urlIMG = "";

export function UploadImage() {
  $.loadScript('https://widget.cloudinary.com/v2.0/global/all.js', function () {
    var myWidget = cloudinary.createUploadWidget({
      cloudName: 'soservices',
      uploadPreset: 'soservices_upload'
    }, (error, result) => {
      if (!error && result && result.event === "success") {
        urlIMG = result.info["url"];
        console.log('Done! Here is the image info: ', urlIMG);
        store.dispatch('ChangeUrl', {url: urlIMG})
      }
    })

    myWidget.open();
  });
}