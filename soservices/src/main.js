import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import 'vuetify/dist/vuetify.min.css'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyA1BtIj_Cu0HVsKxEAwXbayAORWtesIj-0",
    libraries: "places"
  }
})
Vue.use(Vuex)


const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/',
    components: require('./components/Home')
  },
  {
    path: '/Login',
    components: require('./components/Login')
  },
  {
    path: '/Contact',
    components: require('./components/Contact')
  },
  {
    path: '/Account',
    components: require('./components/MyAccount')
  },
  {
    path: '/AnnouncesMaths',
    components: require('./components/AnnouncesMaths')
  },
  {
    path: '/AnnouncesInfos',
    components: require('./components/AnnouncesInfos')
  },
  {
    path: '/AnnouncesDivers',
    components: require('./components/AnnouncesDivers')
  },
  {
    path: '/AddNote',
    components: require('./components/AddNote')
  },
  {
    path: '/AddCat',
    components: require('./components/AddCat')
  },
  {
    path: '/DelNote',
    components: require('./components/DelNote')
  },
  {
    path: '/ExportNote',
    components: require('./components/ExportNote')
  },
  {
    path: '*',
    redirect: '/'
  }
]
})

Vue.config.productionTip = false

new Vue({
  router,
  store : require('./components/SOServicesStore.js'),
  render: h => h(App)
}).$mount('#app')

