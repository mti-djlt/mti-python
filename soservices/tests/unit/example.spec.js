import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import ContactHeader from '@/components/HomeDetails.vue'
import ContactUs from'@/components/GetInTouch.vue'
import Header from'@/components/AppHeader.vue'
import Home from'@/components/Home.vue'
import Login from'@/components/Login.vue'


describe('HomeDetails.vue', () => {
  it('renders props.title when passed', () => {
    const title = 'new message'
    const wrapper = shallowMount(ContactHeader, {
      propsData: { title }
    })
    expect(wrapper.text()).to.include(title)
  })
})

describe('GetInTouch.vue', () => {
  it('testing btn', () => {
    const wrapper = shallowMount(ContactUs)
    expect(wrapper.contains('v-btn')).equals(true)
  })
})

describe('GetInTouch.vue', () => {
  it('Nous contacter button is visible', () => {
    const wrapper = shallowMount(ContactUs)
    expect(wrapper.text()).equals('Nous contacterContact*indiques les champs obligatoiresFermerEnvoyer')
  })
})

describe('GetInTouch.vue', () => {
  it('throw Dialog after dialog attribute set to true', () => {
    const dialogFalse = false
    const wrapper = shallowMount(ContactUs, {
      propsData: { 
        dialogFalse
      }
    })
    const {vm} = wrapper
    const dialog = vm.$refs.myDialog
    expect(dialog).not.to.Throw
    wrapper.setProps({dialog : true})
    expect(dialog).to.Throw
  })
})

describe('GetInTouch.vue', () => {
  it('Click at Nous contacter & throw dialog', () => {
    const wrapper = shallowMount(ContactUs)
    let button = wrapper.find({ ref: 'myButton' });
    expect(button.exists()).equals(true); 
    const {vm} = wrapper
    const dialog = vm.$refs.myDialog
    expect(dialog).not.to.Throw
    button.trigger('click')
    expect(dialog).to.Throw
  })
})

describe('AppHeader.vue', () => {
  it('Click at se connecter & throw dialog', () => {
    const wrapper = shallowMount(Header)
    let button = wrapper.find({ ref: 'loginButton' });
    expect(button.exists()).equals(true); 
    const {vm} = wrapper
    const dialog = vm.$refs.loginDialog
    expect(dialog).not.to.Throw
    button.trigger('click')
    expect(dialog).to.Throw
  })
})

describe('AppHeader.vue', () => {
  it('Click at se connecter & not throw dialog', () => {
    const wrapper = shallowMount(Header)
    let button = wrapper.find({ ref: 'loginButton' });
    expect(button.exists()).equals(true); 
    const {vm} = wrapper
    const dialog = vm.$refs.loginDialog
    expect(dialog).not.to.Throw
    button.trigger('click')
    expect(dialog).not.to.Throw
  })
})

describe('AppHeader.vue', () => {
  it('look at the button s\'inscrire & click without showing dialog', () => {
    const wrapper = shallowMount(Header)
    let button = wrapper.find({ ref: 'loginButton' });
    expect(button.exists()).equals(true); 
    const {vm} = wrapper
    const dialog = vm.$refs.loginDialog
    expect(dialog).not.to.Throw
    button.trigger('click')
    expect(dialog).to.Throw
  })
})

/*describe('Home.vue', () => {
  it('look at a ref & throw error "undefined ref"', () => {
    const wrapper = shallowMount(Home)
    let h1 = wrapper.find({ ref: 'testH1' });
    expect(h1.exists()).equals(true); 
  })
})*/

describe('Login.vue', () => {
  it('checkbox checking', () => {
    const wrapper = shallowMount(Login)
    let checkbox = wrapper.find({ ref: 'checkbox1' });
    expect(checkbox.exists()).equals(true); 
  })
})

describe('Login.vue', () => {
  it('checkbox2 checking', () => {
    const wrapper = shallowMount(Login)
    let checkbox = wrapper.find({ ref: 'checkbox2' });
    expect(checkbox.exists()).equals(true); 
    checkbox.trigger('click')
  })
})

describe('Login.vue', () => {
  it('button open file checking', () => {
    const wrapper = shallowMount(Login)
    let checkbox = wrapper.find({ ref: 'buttonFile' });
    expect(checkbox.exists()).equals(true); 
  })
})

